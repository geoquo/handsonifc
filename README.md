# Hands-on IFC
Hands-on IFC is an open-sourec (AGPL) library to write non-CAD geometries, including the information about them to the [Industry Foundation Classses](https://technical.buildingsmart.org/standards/ifc/) (IFC) file format. Hands-on IFC is based on IfcOpenShell, an open-source (LGPL) library to work with the IFC file format. IfcOpenShell currently supports IFC version [IFC2x3](https://standards.buildingsmart.org/IFC/RELEASE/IFC2x3/TC1/HTML/) (ISO 16739-1:2018) und [IFC4 ADD2 TC1](https://standards.buildingsmart.org/IFC/RELEASE/IFC4/ADD2_TC1/HTML/) (ISO/PAS 16739:2005). Additional information can be found at [IFC Open Shell](http://ifcopenshell.org), [IFC Academy](http://academy.ifcopenshell.org) or in the [GitHub](https://github.com/IfcOpenShell/IfcOpenShell) repository. The GeoQuo GmbH acknowledges the great ['Simple Wall'](http://academy.ifcopenshell.org/creating-a-simple-wall-with-property-set-and-quantity-information/) example of Kianwee Chen (ETH), which served as an inspiration for the module presented here. Also acknowledged is the outstanding support provided by David Berger, answering various technical questions.

### Setup
Hands-on IFC is a command line interface (CLI), based on python libraries. The provided Docker image allows the plattform independent usage of the library in an reproducible Python 3.8.11 environment on Debian 10 (buster).

```sh
python -m venv .venv && source .venv/bin/activate && pip install --upgrade pip
pip install -r libraries/requirements.txt
pip install -e libraries/ifcopenshell
pip install -e libraries/handsonifc
```

### Usage
Show all available commands:

```sh
python -m handsonifc --help
```

List all options of the handsonifc cammand createIfc:

```sh
python -m handsonifc createIfc --help
```

### Examples
Terrain surfaces can be created by using e.g. [SAGA GIS](http://www.saga-gis.org) to convert raster data to [Triangulated Irregular Networks (TINs)](https://en.wikipedia.org/wiki/Triangulated_irregular_network). For the example below, the TIN was further converted to the [Standard Tessellation Language (STL)](https://en.wikipedia.org/wiki/STL_(file_format)).

![mountain.stl](assets/mountain.png)

The handsonifc command createIfc allows writing the geometry to an IfcBuildingElementProxy linked to IfcSite.

```sh
python -m handsonifc createIfc --path "./data/inputs/mountain.stl"
python -m handsonifc createIfc --path "./data/inputs/"
```
# ![mountain.ifc](assets/solibri.png)