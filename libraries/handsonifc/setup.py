#######################
# handsonifc setup.py #
#######################

from setuptools import setup, find_packages

setup(
    name="handsonifc",
    version="0.0.1",
    description="",
    packages=find_packages(),
    install_requires=["click", "ifcopenshell"],
    python_requires=">=3.8",
)
