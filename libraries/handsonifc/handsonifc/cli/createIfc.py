import os
import os.path
import time

from handsonifc.ifc import (
    new_ifcfile,
    create_guid,
    create_ifclocalplacement,
    create_ifcaxis2placement,
    create_ifcpolyline,
    create_ifcextrudedareasolid,
)

from handsonifc.utils import measure_time
from handsonifc.reader import get_geom_reader, get_sidecar_reader

@measure_time
def writeGeometryToProxy(ifcfile, context, owner_history, proxy_placement, site, cartesianPointList, triangulatedFaceSet, path, filename, sidecar):

    fullpath = os.path.join(path,filename + sidecar)
    reader_class = get_sidecar_reader(sidecar)
    reader = reader_class()
    result = reader.read(fullpath)
    
    cords = ifcfile.createIfcCartesianPointList3d(cartesianPointList)
    faces = ifcfile.createIfcTriangulatedFaceSet(cords, None, None, triangulatedFaceSet, None)

    # Colour geometries
    rgb = [x / 255 for x in result['color_rgb']]
    colour = ifcfile.createIfcColourRgb(None,rgb[0],rgb[1],rgb[2])
    style_shading = ifcfile.createIfcSurfaceStyleShading(colour,None)
    surface_style = ifcfile.createIfcSurfaceStyle(None,"BOTH",(style_shading,))
    style_assignment = ifcfile.createIfcPresentationStyleAssignment((surface_style,))
    styled_item = ifcfile.createIfcStyledItem(faces,(style_assignment,),None)

    body_representation = ifcfile.createIfcShapeRepresentation(context, "Body", "SweptSolid", [faces])

    # product_shape = ifcfile.createIfcProductDefinitionShape(None, None, [body_representation, styled_item])
    product_shape = ifcfile.createIfcProductDefinitionShape(None, None, [body_representation])
    proxy = ifcfile.createIfcBuildingElementProxy(create_guid(), owner_history, filename, None, None, proxy_placement, product_shape, None)
    
    guids = []
    for index, pset in enumerate(result['psets']):
        property_values = []
        guids.append(create_guid())
        for attr in pset['attributes']:
            entry = ifcfile.createIfcPropertySingleValue(attr['name'], attr['name'], ifcfile.create_entity(attr['type'], attr['value']), None),
            property_values.append(entry[0])
            property_set = ifcfile.createIfcPropertySet(guids[index], owner_history, pset['name'], None, property_values)
    
        ifcfile.createIfcRelDefinesByProperties(create_guid(), owner_history, None, None, [proxy], property_set)
    
    ifcfile.createIfcRelContainedInSpatialStructure(create_guid(), owner_history, "Building Storey Container", None, [proxy], site)

def createIfcCmd(path, destination, filename, creator, organization, project_name, sidecar, shift_x, shift_y, shift_z):

    with new_ifcfile(
        path, destination, filename, creator, organization, project_name, sidecar, shift_x, shift_y, shift_z
    ) as ifcfile:
        owner_history = ifcfile.by_type("IfcOwnerHistory")[0]
        context = ifcfile.by_type("IfcGeometricRepresentationContext")[0]
        site = ifcfile.by_type("IfcSite")[0]
        building = ifcfile.by_type("IfcBuilding")[0]
        site_placement = site.ObjectPlacement
        building_placement = building.ObjectPlacement

        storey_placement = create_ifclocalplacement(ifcfile, relative_to=building_placement)
        building_storey = ifcfile.createIfcBuildingStorey(create_guid(), owner_history, "Storey", None, None, storey_placement, None, None, "ELEMENT", 0.0)
        ifcfile.createIfcRelAggregates(create_guid(), owner_history, "Building Container", None, building, [building_storey])
        proxy_placement = create_ifclocalplacement(ifcfile, point=(0.0, 0.0, 0.0), relative_to=site_placement)
        
        # Read files
        if not os.path.isdir(path):
            filelist = [os.path.basename(path)]
            path = os.path.dirname(path)
        else:
            filelist = os.listdir(path)
       
        # Create transformation vector
        transform = [shift_x, shift_y, shift_z]

        totalNoFaces = 0
        report = []
        for inputFile in filelist:
            [filename, extension] = os.path.splitext(inputFile)
            if extension in ['.dxf','.obj','.ts','.stl']:
                fullpath = os.path.join(path,filename + extension)
                reader_class = get_geom_reader(extension)
                reader = reader_class(transform)
                vertices, faces = reader.read(fullpath)
                (_, time) = writeGeometryToProxy(ifcfile, context, owner_history, proxy_placement, site, vertices, faces, path, filename, sidecar)
                totalNoFaces += len(faces)
                # print(totalNoFaces)
                seconds = "%.2f" % round(time, 2)
                text = seconds + " seconds to transform " + filename + extension
                feedback = text + " (" + str(totalNoFaces) + " faces)"
                print(feedback)
                report.append(feedback)

        # return totalNoFaces

    return report


