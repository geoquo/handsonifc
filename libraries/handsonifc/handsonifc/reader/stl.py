import time
from .base import GeometryReader


class STLReader(GeometryReader):

    def _read(self, path):
        # Conversion to nested list of triangles:
        # [triangle [vertex [x,y,z]],[vertex [x,y,z]],[vertex [x,y,z]]]
        triangles = []
        with open(path) as f:
            while True:
                line = f.readline().strip()
                if not line:
                    break
                if line == "outer loop":
                    triangle = [f.readline().strip() for i in range(3)]
                    vertices = []
                    for i in range(3):
                        vertex = triangle[i].rstrip().split(" ")[1:]
                        vertex = [float(i) for i in vertex]
                        vertices.append(vertex)
                    triangles.extend([vertices])

        # Create CartesianPointList Tuple
        vertices = []
        for i in range(len(triangles)):
            for j in range(3):
                vertex = tuple(triangles[i][j])
                vertices.append(vertex)

        # Create IfcTriangulatedFaceSet Tuple
        faces = []
        for i in range(len(triangles)):
            face = (i * 3 + 1, i * 3 + 2, i * 3 + 3)
            faces.append(face)

        return vertices, faces